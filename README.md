# Portfolio project IDATT1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = Josefine Arntsen  
STUDENT ID = 111782

## Project description

[//]: # (TODO: Write a short description of your project/product here.)
This project is an implementation of a train departure system in Java. 
The system allows users to see an overview of train departures, add new trains, 
change track numbers and current time, add delays, 
and search for trains based on train number and destination.

## Project structure

[//]: # (TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)
The project is organized in the following structure:

src: The source code for the project, including the implementation of the TrainDeparture and TrainDepartureRegister classes.

test: The folder containing JUnit test classes to test the functionality of the project classes.

## Link to repository

[//]: # (TODO: Include a link to your repository here.)
https://gitlab.stud.idi.ntnu.no/josefiar/idatt1003-2023-mappe-traindispatchsystem

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
To run the project, follow these steps:

Open the project in a Java IDE like IntelliJ.

Find and run the main class TrainDispatchApp.

Follow the instructions displayed in the terminal/console to interact with the train departure system.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
How to run the tests:

Open the project in a Java IDE.

Navigate to the test folder.

Right-click on the JUnit test classes and select "Run" to execute the tests.

## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
ChatGPT is used for errors and javadoc.

No other specific references were used in this project
