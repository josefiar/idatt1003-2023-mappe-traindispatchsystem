package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class contains JUnit tests for the TrainDeparture class.
 */
@DisplayName("TrainDeparture Test of entity class")
public class TestTrainDeparture {
  LocalTime departureTime;
  String line;
  int trainNumber;
  String destination;
  Duration delay;
  int track;
  TrainDeparture testTrain1;

  /**
   * Sets up a train departure for testing.
   */
  @BeforeEach
  void setUp() {
    departureTime = LocalTime.of(12, 15);
    line = "L1";
    trainNumber = 601;
    destination = "Oslo";
    delay = Duration.ofMinutes(15);
    track = 2;

    testTrain1 = new TrainDeparture(departureTime, line, trainNumber, destination);
  }

  /**
   * Nested class for testing the TrainDeparture constructor.
   */
  @Nested
  @DisplayName("TrainDeparture Constructor")
  public class TrainDepartureConstructor {

    /**
     * Tests the constructor with valid parameters.
     */
    @Test
    @DisplayName("Test valid constructor parameters")
    void testValidConstructorParameters() {
      // Arrange: Valid parameters are set up
      // Act: A TrainDeparture instance is created (testTrain1)

      // Assert: Verify that the object was created successfully
      assertDoesNotThrow(() -> new TrainDeparture(
          departureTime, line, trainNumber, destination));
      assertNotNull(testTrain1,
          "TrainDeparture instance should not be null");
      assertEquals(departureTime, testTrain1.getDepartureTime(),
          "Departure time should match");
      assertEquals(line, testTrain1.getLine(),
          "Line should match");
      assertEquals(trainNumber, testTrain1.getTrainNumber(),
          "Train number should match");
      assertEquals(destination, testTrain1.getDestination(),
          "Destination should match");
    }

    /**
     * Tests the constructor with a valid delay parameter.
     */
    @Test
    @DisplayName("Test valid constructor parameter delay")
    void testValidConstructorDelay() {
      // Arrange: A valid delay parameter is already set up

      // Act: Set the delay parameter using the setter
      testTrain1.setDelay(delay);

      // Assert: Verify that the delay parameter matches the set value
      assertEquals(delay, testTrain1.getDelay(),
          "Delay should match the set value");
    }

    /**
     * Tests the constructor with a valid track parameter.
     */
    @Test
    @DisplayName("Test valid constructor parameter track")
    void testValidConstructorTrack() {
      // Arrange: A valid track parameter is already set up

      // Act: Set the track parameter using the setter
      testTrain1.setTrack((track));

      // Assert: Verify that the track parameter matches the expected value
      assertEquals(track, testTrain1.getTrack(),
          "Track should match the set value");
    }

    /**
     * Tests the constructor with invalid parameters.
     */
    @Test
    @DisplayName("Test invalid constructor parameters")
    void testInvalidConstructorParameters() {
      // Arrange, Act and Assert:
      // Verify that IllegalArgumentException is thrown for each invalid parameter
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture(null, line, trainNumber, destination),
          "Should throw IllegalArgumentException for null departureTime");
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture(departureTime, null, trainNumber, destination),
          "Should throw IllegalArgumentException for null line");
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture(departureTime, line, -123, destination),
          "Should throw IllegalArgumentException for negative trainNumber");
      assertThrows(IllegalArgumentException.class,
          () -> new TrainDeparture(departureTime, line, trainNumber, null),
          "Should throw IllegalArgumentException for null destination");
    }
  }

  /**
   * Nested class for testing the {@code isDeparted()} method of the TrainDeparture class.
   */
  @Nested
  @DisplayName("TrainDeparture isDeparted() method")
  class TestIsDepartedMethod {

    /**
     * Tests the {@code isDeparted()} method when the train has not departed yet.
     */
    @Test
    @DisplayName("Positive Test - Not Departed Yet")
    void testIsDepartedNotDeparted() {

      // Arrange: Set initial delay
      testTrain1.setDelay(delay);

      // Calculate the expected departure time with delay
      LocalTime expectedDepartureWithDelay = departureTime.plusMinutes(15);

      // Act and Assert:
      // Call isDeparted() with a time before expected departure time
      assertFalse(testTrain1.isDeparted(expectedDepartureWithDelay
              .minusMinutes(1)),
          "Train should not be departed yet");
    }

    /**
     * Tests the {@code isDeparted()} method when the train has already departed.
     */
    @Test
    @DisplayName("Positive Test - Already Departed")
    void testIsDepartedDeparted() {
      // Arrange: Set initial delay
      testTrain1.setDelay(delay);

      // Calculate the expected departure time with delay
      LocalTime expectedDepartureWithDelay = departureTime.plusMinutes(15);

      // Act and Assert:
      // Call isDeparted() with a time after expected departure time
      assertTrue(testTrain1.isDeparted(expectedDepartureWithDelay
              .plusMinutes(1)),
          "Train should be departed");
    }
  }

  /**
   * Nested class for testing the getter methods of the TrainDeparture class.
   */
  @Nested
  @DisplayName("TrainDeparture get()-methods")
  class TrainDepartureGetMethods {

    /**
     * Tests the {@code getAdjustedDepartureTime()} method with positive delay.
     */
    @Test
    @DisplayName("Test getAdjustedDepartureTime() with positive delay")
    void testGetAdjustedDepartureTimePositiveDelay() {
      // Arrange: Already created a TrainDeparture instance; testTrain1
      testTrain1.setDelay(delay);

      // Act: Call getAdjustedDepartureTime() with positive delay
      LocalTime adjustedDepartureTime = testTrain1.getAdjustedDepartureTime();

      // Assert: Verify that the adjusted departure time matches the expected time
      assertEquals(departureTime.plusMinutes(15), adjustedDepartureTime,
          "The adjusted departure time should consider the positive delay");
    }

    /**
     * Tests the {@code getAdjustedDepartureTime()} method with zero delay.
     */
    @Test
    @DisplayName("Test getAdjustedDepartureTime() with zero delay")
    void testGetAdjustedDepartureTimeZeroDelay() {
      // Arrange: Already created a TrainDeparture instance; testTrain1
      testTrain1.setDelay(Duration.ZERO);

      // Act: Call getAdjustedDepartureTime() with zero delay
      LocalTime adjustedDepartureTime = testTrain1.getAdjustedDepartureTime();

      // Assert: Verify that the adjusted departure time matches the original departure time
      assertEquals(departureTime, adjustedDepartureTime,
          "The adjusted departure time should be the same as the original departure time with zero delay");
    }

    /**
     * Tests the {@code getDepartureTime()} method.
     */
    @Test
    @DisplayName("Test - Get Departure Time")
    void testGetDepartureTime() {
      // Arrange: Departure time is already set during setup

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected departure time
      assertEquals(departureTime, testTrain1.getDepartureTime(),
          "Departure time should match the set value");

      // Negative Test: Verify that the getter does not return null
      assertNotNull(testTrain1.getDepartureTime(),
          "Departure time should not be null");
    }

    /**
     * Tests the {@code getLine()} method.
     */
    @Test
    @DisplayName("Test - Get Line")
    void testGetLine() {
      // Arrange: Line is already set during setup

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected line designation
      assertEquals(line, testTrain1.getLine(),
          "Line should match the set value");

      // Negative Test: Verify that the getter does not return null
      assertNotNull(testTrain1.getLine(),
          "Line designation should not be null");
    }

    /**
     * Tests the {@code getTrainNumber()} method.
     */
    @Test
    @DisplayName("Test - Get Train Number")
    void testGetTrainNumber() {
      // Arrange: Train number is already set during setup

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected train number
      assertEquals(trainNumber, testTrain1.getTrainNumber(),
          "Train number should match the set value");

      // Negative Test: Verify that the getter does not return a negative value
      assertTrue(testTrain1.getTrainNumber() >= 0,
          "Train number should not be negative");
    }

    /**
     * Tests the {@code getDestination()} method.
     */
    @Test
    @DisplayName("Test - Get Destination")
    void testGetDestination() {
      // Arrange: Destination is already set during setup

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected destination
      assertEquals(destination, testTrain1.getDestination(),
          "Destination should match the set value");

      // Negative Test: Verify that the getter does not return null
      assertNotNull(testTrain1.getDestination(),
          "Destination should not be null");
    }

    /**
     * Tests the {@code getDelay()} method with a valid delay set.
     */
    @Test
    @DisplayName("Test - Get Delay with Valid Delay Set")
    void testGetDelayValidDelaySet() {
      // Arrange: Set delay
      testTrain1.setDelay(delay);

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected delay
      assertEquals(delay, testTrain1.getDelay(),
          "Delay should match the set value");

      // Negative Test: Verify that the getter does not return null
      assertNotNull(testTrain1.getDelay(),
          "Delay should not be null");
    }

    /**
     * Tests the {@code getDelay()} method with no delay set.
     */
    @Test
    @DisplayName("Test - Get Delay with No Delay Set")
    void testGetDelayNoDelaySet() {
      // Arrange: Ensure no delay is set
      testTrain1.setDelay(Duration.ZERO);

      // Act and Assert:
      // Verify that the getter returns Duration.ZERO when no delay is set
      assertEquals(Duration.ZERO, testTrain1.getDelay(),
          "Delay should be Duration.ZERO when no delay is set");
    }

    /**
     * Tests the {@code getTrack()} method with a valid track set.
     */
    @Test
    @DisplayName("Test - Get Track with Valid Track Set")
    void testGetTrackValidTrackSet() {
      // Arrange: Set track
      testTrain1.setTrack(track);

      // Act and Assert:
      // Positive Test: Verify that the getter returns the expected track value
      assertEquals(track, testTrain1.getTrack(),
          "Track should match the set value");
    }

    /**
     * Tests the {@code getTrack()} method with no track set.
     */
    @Test
    @DisplayName("Test - Get Track with No Track Set")
    void testGetTrackNoTrackSet() {
      // Arrange: Ensure no track is set
      testTrain1.setTrack(-1);

      // Act and Assert:
      // Negative Test: Verify that the getter returns -1 if no track is set

      assertEquals(-1, testTrain1.getTrack(),
          "Track should be -1 when no track is set");
    }
  }

  /**
   * Tests for the setter methods of the {@link TrainDeparture} class.
   */
  @Nested
  @DisplayName("TrainDeparture setter methods")
  class TrainDepartureSetMethods {

    /**
     * Tests the {@code delayedDepartureTime()} method with a positive delay.
     */
    @Test
    @DisplayName("Test delayedDepartureTime() with positive delay")
    void testAddDelayToDepartureTimePositiveDelay() {
      // Arrange: Set initial delay
      testTrain1.setDelay(delay);

      // Act: Call addDelayToDepartureTime() to update departureTime based on the initial delay
      testTrain1.addDelayToDepartureTime();

      // Assert: Verify that departureTime is updated correctly
      assertEquals(departureTime.plusMinutes(15),
          testTrain1.getDepartureTime(),
          "Departure time should be updated with the positive delay");
    }


    /**
     * Tests the {@code setDelay()} method with a positive delay.
     */
    @Test
    @DisplayName("Test setDelay() with positive delay")
    void testSetDelayPositiveDelay() {
      // Act: Call setDelay() to add a positive delay
      assertDoesNotThrow(() -> testTrain1.setDelay(delay));

      // Assert: Verify that the delay is set correctly
      assertEquals(delay, testTrain1.getDelay(),
          "Delay should match the set value");
    }

    /**
     * Tests the {@code setDelay()} method with a negative delay.
     */
    @Test
    @DisplayName("Test setDelay() with negative delay")
    void testSetDelayNegativeDelay() {
      // Arrange: Create negative delay
      Duration negativeDelay = Duration.ofMinutes(-15);

      // Act and Assert: Call setDelay() with negative additional delay
      IllegalArgumentException exception =
          assertThrows(IllegalArgumentException.class,
          () -> testTrain1.setDelay(negativeDelay),
          "Should throw IllegalArgumentException for negative additional delay");

      assertEquals("Additional delay cannot be negative.",
          exception.getMessage(),
          "Exception message should indicate the reason for failure");

      // Assert: Verify that the delay remains unchanged for negative delay
      assertEquals(Duration.ZERO, testTrain1.getDelay(),
          "Delay should remain zero for negative delay");
    }

    /**
     * Tests the {@code setTrack()} method of the TrainDeparture class.
     * Positive test: Set a valid positive track.
     */
    @Test
    @DisplayName("Test - Set Valid Positive Track")
    void testSetValidPositiveTrack() {
      // Arrange: Already created a TrainDeparture instance; testTrain1

      // Act: Set a valid track using the setTrack method
      int validTrack = 3;
      testTrain1.setTrack(validTrack);

      // Assert: Verify that the track has been set correctly
      assertEquals(validTrack, testTrain1.getTrack(),
          "Track should match the set value");
    }

    /**
     * Negative test: Attempt to set an invalid negative track.
     */
    @Test
    @DisplayName("Test - Set Invalid Negative Track")
    void testSetInvalidNegativeTrack() {
      // Arrange: Already created a TrainDeparture instance; testTrain1

      // Act and Assert: Verify that IllegalArgumentException is thrown
      int invalidTrack = -2;
      IllegalArgumentException exception =
          assertThrows(IllegalArgumentException.class,
              () -> testTrain1.setTrack(invalidTrack),
              "Should throw IllegalArgumentException for negative track");

      assertEquals("Invalid track number. Has to be a positive number or -1.",
          exception.getMessage(),
          "Exception message should indicate the reason for failure");
    }

    /**
     * Negative test: Attempt to set an invalid track (positive but not equal to -1).
     */
    @Test
    @DisplayName("Test - Set Invalid Positive Track")
    void testSetInvalidPositiveTrack() {
      // Arrange: Already created a TrainDeparture instance; testTrain1

      // Act and Assert: Verify that IllegalArgumentException is thrown
      int invalidPositiveTrack = 0;
      IllegalArgumentException positiveException =
          assertThrows(IllegalArgumentException.class,
              () -> testTrain1.setTrack(invalidPositiveTrack),
              "Should throw IllegalArgumentException for invalid positive track");

      assertEquals("Invalid track number. Has to be a positive number or -1.",
          positiveException.getMessage(),
          "Exception message should indicate the reason for failure");
    }
  }

  /**
   * Tests for the {@code toString()} method of the {@link TrainDeparture} class.
   */
  @Nested
  @DisplayName("TrainDeparture toString() Method")
  class TrainDepartureToString {

    /**
     * Tests the {@code toString()} method with no delay and no track.
     */
    @Test
    @DisplayName("Test toString() with no delay and no track")
    void testToStringNoDelayNoTrack() {
      // Arrange: Already created a TrainDeparture instance; testTrain1

      // Act: Call toString() with no delay and no track
      String resultString = testTrain1.toString();

      // Assert: Verify the result against the expected string
      String expectedString =
          "|  12:15               |  L1        |  601            |  Oslo                |            |            |                          |\n";

      assertEquals(expectedString, resultString,
          "The toString() result should match the expected string for no delay and no track");
    }

    /**
     * Tests the {code toString()} method with delay and track.
     */
    @Test
    @DisplayName("Test toString() with delay and track")
    void testToStringWithDelayAndTrack() {
      // Arrange: Already created a TrainDeparture instance; testTrain1
      // Set delay and track
      testTrain1.setDelay(delay);
      testTrain1.setTrack(track);

      // Act: Call toString() with delay and track
      String resultString = testTrain1.toString();

      // Assert: Verify the result against the expected string
      String expectedString =
          "|  12:15               |  L1        |  601            |  Oslo                |  0h, 15m   |  2         |   12:30                  |\n";

      assertEquals(expectedString, resultString,
          "The toString() result should match the expected string for delay and track");
    }
  }
}
