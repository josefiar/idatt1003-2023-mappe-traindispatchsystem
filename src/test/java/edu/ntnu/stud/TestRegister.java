package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test class for {@link TrainDepartureRegister}.
 */
@DisplayName("TrainDepartureRegister Test")
public class TestRegister {

  private TrainDepartureRegister trainDepartureRegister;
  TrainDeparture newDeparture;
  LocalTime departureTime;
  String line;
  int trainNumber;
  String destination;
  LocalTime delay;
  int track;
  TrainDeparture departure1;
  TrainDeparture departure2;
  TrainDeparture departure3;

  /**
   * Sets up the test environment before each test method is executed.
   * Initializes a new instance of {@link TrainDepartureRegister} and creates
   * several {@link TrainDeparture} instances with predefined values. These
   * instances are added to the train departure register for testing purposes.
   */
  @BeforeEach
  void setUp() {
    trainDepartureRegister = new TrainDepartureRegister();

    departureTime = LocalTime.of(14, 30);
    line = "L2";
    trainNumber = 602;
    destination = "Trondheim";
    delay = LocalTime.of(1, 15);
    track = 5;

    // Create TrainDeparture instances
    newDeparture = new TrainDeparture(departureTime, line, trainNumber, destination);
    departure1 = new TrainDeparture(
        LocalTime.of(14, 0), "L2", 601, "Trondheim");
    departure2 = new TrainDeparture(
        LocalTime.of(15, 45), "L3", 603, "Bergen");
    departure3 = new TrainDeparture(
        LocalTime.of(17, 0), "L4", 604, "Bergen");

    // Add TrainDeparture instances to the register
    trainDepartureRegister.addTrainDeparture(newDeparture);
    trainDepartureRegister.addTrainDeparture(departure1);
    trainDepartureRegister.addTrainDeparture(departure2);
    trainDepartureRegister.addTrainDeparture(departure3);
  }

  /**
   * Nested class for testing the addition of new train departures to the register.
   */
  @Nested
  @DisplayName("Adding a new train departure")
  public class AddDepartureRegister {

    /**
     * Tests adding a new valid train departure to the register.
     */
    @Test
    @DisplayName("Test adding a new train departure")
    void testAddTrainDeparture() {
      // Arrange
      TrainDeparture departure4 = new TrainDeparture(
          LocalTime.of(20, 0), "L5", 700, "Stavanger");

      // Act
      boolean addedSuccessfully = trainDepartureRegister.addTrainDeparture(departure4);

      // Assert
      assertTrue(addedSuccessfully,
          "Adding a new train departure should return true");
      assertEquals(5, trainDepartureRegister.getTrainDepartures().size(),
          "The number of train departures should be 5 after addition");
      assertTrue(trainDepartureRegister.getTrainDepartures().contains(departure4),
          "The newly added train departure should be in the register");
    }

    /**
     * Tests adding a train with the same train number as an existing departure.
     * Expects an IllegalArgumentException to be thrown with the appropriate message.
     */
    @Test
    @DisplayName("Test adding a train with the same train number")
    void testAddDuplicateTrainNumber() {
      // Arrange
      TrainDeparture duplicateDeparture
          = new TrainDeparture(departureTime, line, trainNumber, destination);

      // Act and Assert
      IllegalArgumentException exception =
          assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.addTrainDeparture(duplicateDeparture),
          "Adding a train with the same train number should throw an IllegalArgumentException");

      assertEquals("Train departure with the same train number already exists.",
          exception.getMessage(),
          "Exception message should indicate that a train with the same number already exists");

      assertEquals(4, trainDepartureRegister.getTrainDepartures().size(),
          "The number of train departures should remain 4 after a failed addition");
    }

    /**
     * Tests adding a new valid train departure to the register from UI.
     */
    @Test
    @DisplayName("Test adding a train from UI")
    void testAddTrainDepartureFromUIValid() {
      // Positive test with valid parameters
      LocalTime departureTime = LocalTime.of(9, 0);
      String line = "L6";
      int trainNumber = 800;
      String destination = "Stavanger";
      trainDepartureRegister.addTrainDepartureFromUI(departureTime, line, trainNumber, destination);

      // Assert
      assertEquals(5, trainDepartureRegister.getTrainDepartures().size(),
          "The number of train departures should be 5 after addition");

      TrainDeparture addedDeparture = trainDepartureRegister.getTrainDepartures().get(4);

      assertNotNull(addedDeparture);
      assertTrue(trainDepartureRegister.getTrainDepartures().contains(addedDeparture),
          "The newly added train departure should be in the register");
      assertEquals(departureTime, addedDeparture.getDepartureTime());
      assertEquals(line, addedDeparture.getLine());
      assertEquals(trainNumber, addedDeparture.getTrainNumber());
      assertEquals(destination, addedDeparture.getDestination());
    }
  }

  /**
   * Nested class for testing the find methods for destination
   * and train number of train departures to the register.
   */
  @Nested
  @DisplayName("Register Find-methods")
  public class RegisterFindMethods {

    /**
     * Test finding a train departure by train number (positive scenario).
     */
    @Test
    @DisplayName("Test finding a train departure by train number "
        + "- Positive Scenario")
    void testFindTrainDepartureByTrainNumber() {
      // Arrange and Act
      TrainDeparture foundTrain
          = trainDepartureRegister.findTrainDepartureByTrainNumber(602);

      //Assert
      assertNotNull(foundTrain,
          "The found train should not be null. ");
      assertEquals(newDeparture, foundTrain,
          "The found train should match the expected departure.");
    }

    /**
     * Test finding a train departure by train number when
     * the train number does not exist (negative scenario).
     */
    @Test
    @DisplayName("Test finding a train departure by train number "
        + "- Negative Scenario (Nonexistent Train Number)")
    void testFindTrainDepartureByTrainNumberNonexistent() {
      // Arrange and Act
      TrainDeparture foundTrain
          = trainDepartureRegister.findTrainDepartureByTrainNumber(999);

      // Assert
      assertNull(foundTrain,
          "No train should be found for a nonexistent train number.");
    }

    /**
     * Test finding a train departure by train number
     * with an invalid (negative) train number.
     */
    @Test
    @DisplayName("Test finding a train departure by train number "
        + "- Negative Scenario (Invalid Train Number)")
    void testFindTrainDepartureByTrainNumberInvalid() {
      // Arrange and Act
      TrainDeparture foundTrain
          = trainDepartureRegister.findTrainDepartureByTrainNumber(-1);

      // Assert
      assertNull(foundTrain,
          "No train should be found for an invalid (negative) train number.");
    }

    /**
     * Test finding train departures by destination (positive scenario).
     */
    @Test
    @DisplayName("Test finding train departures by destination "
        + "- Positive Scenario")
    void testFindTrainDeparturesByDestination() {
      // Arrange and Act
      ArrayList<TrainDeparture> foundDepartures
          = trainDepartureRegister.findTrainDeparturesByDestination("Bergen");

      // Assert
      assertAll("Found departures by destination",
          () -> assertEquals(2, foundDepartures.size(),
              "Incorrect number of departures found"),
          () -> assertTrue(foundDepartures.contains(departure2),
              "Departure2 not found in results"),
          () -> assertTrue(foundDepartures.contains(departure3),
              "Departure3 not found in results")
      );
    }

    /**
     * Test finding train departures by destination with null destination (negative scenario).
     */
    @Test
    @DisplayName("Test finding train departures by destination"
        + "- Negative Scenario (Null Destination)")
    void testFindTrainDeparturesByDestinationNullDestination() {
      // Assert
      assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.findTrainDeparturesByDestination(null),
          "Expected IllegalArgumentException not thrown");
      assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.findTrainDeparturesByDestination("NonexistentCity"),
          "Expected IllegalArgumentException not thrown");
    }
  }

  /**
   * Nested class for testing the removal of departed trains from the register.
   */
  @Nested
  @DisplayName("Register remove train if departed")
  public class RegisterRemoveTrain {

    /**
     * Test removing departed trains based on the current time (positive scenario).
     * Assumes that the initial current time is set to a value after the first departures.
     */
    @Test
    @DisplayName("Test removing departed trains based on current time")
    void testRemoveDepartedDepartures() {

      // Arrange: Set the current time to a value after the first departures
      // Act: The setNewCurrentTime() method also removes departed departures
      trainDepartureRegister.setNewCurrentTime(LocalTime.of(16, 0));

      // Assert: Validate the state of the register after removal
      assertEquals(1, trainDepartureRegister.getTrainDepartures().size(),
          "The number of train departures should be 1 after removing departed departures");
      assertFalse(trainDepartureRegister.getTrainDepartures().contains(departure1),
          "Departure1 should be removed as it has departed");
      assertTrue(trainDepartureRegister.getTrainDepartures().contains(departure3),
          "Departure3 should remain in the register as it has not departed");
    }
  }

  /**
   * Nested class for testing the sorting of train departures by departure time.
   */
  @Nested
  @DisplayName("Register sort train by time")
  public class RegisterSortTrainByTime {

    /**
     * Test getting all train departures sorted by departure time (positive scenario).
     * The initial state of the register is set up with multiple train departures.
     */
    @Test
    @DisplayName("Test getting all train departures sorted by departure time - Positive Scenario")
    void testGetAllDeparturesSortedByTime() {
      // Arrange: The initial state of the register is set up with multiple train departures

      // Act: Get all train departures sorted by departure time
      ArrayList<TrainDeparture> sortedDepartures
          = trainDepartureRegister.getAllDeparturesSortedByTime();

      // Assert: Validate the sorted order
      assertAll("Sorted Departures",
          () -> assertEquals(4, sortedDepartures.size(),
              "The number of sorted train departures should be 4"),
          () -> assertEquals(departure1, sortedDepartures.get(0),
          "Departure1 should be the first element in the sorted list"),
          () -> assertEquals(newDeparture, sortedDepartures.get(1),
          "NewDeparture should be the second element in the sorted list"),
          () -> assertEquals(departure2, sortedDepartures.get(2),
          "Departure2 should be the third element in the sorted list"),
          () -> assertEquals(departure3, sortedDepartures.get(3),
          "Departure3 should be the fourth element in the sorted list")
      );
    }

    /**
     * Test getting all train departures sorted by departure time
     * when the register is empty (negative scenario).
     * Expects an empty list to be returned.
     */
    @Test
    @DisplayName("Test getting all train departures sorted by departure time "
        + "with an empty register - Negative Scenario")
    void testGetAllDeparturesSortedByTimeEmptyRegister() {
      // Arrange: Set up an empty train departure register
      TrainDepartureRegister emptyRegister = new TrainDepartureRegister();

      // Act: Get all train departures sorted by departure time
      ArrayList<TrainDeparture> sortedDepartures = emptyRegister.getAllDeparturesSortedByTime();

      // Assert: Validate that the sorted list is empty
      assertTrue(sortedDepartures.isEmpty(),
          "The sorted list should be empty for an empty register");
    }
  }

  /**
   * Nested class for testing the Current Time methods of the TrainDepartureRegister.
   */
  @Nested
  @DisplayName("Test Current Time methods")
  class TestCurrentTime {

    /**
     * Tests the {@code setNewCurrentTime()} method with valid input.
     */
    @Test
    @DisplayName("Test setNewCurrentTime() with valid input")
    void testSetNewCurrentTimeValid() {
      // Arrange: The current time
      LocalTime initialCurrentTime = LocalTime.of(12,0);
      trainDepartureRegister.setNewCurrentTime(initialCurrentTime);

      // Act: New current time
      LocalTime newCurrentTime = LocalTime.of(13,0);

      // Assert: Set a new valid current time and verify if it is updated
      assertAll(
          () -> assertDoesNotThrow(() -> trainDepartureRegister.setNewCurrentTime(newCurrentTime)),
          () -> assertEquals(newCurrentTime, trainDepartureRegister.getCurrentTime(),
              "The current time should be updated after setting a new valid time.")
      );
    }

    /**
     * Tests the {@code setNewCurrentTime()} method with null input.
     * Expects an IllegalArgumentException to be thrown with the appropriate message.
     */
    @Test
    @DisplayName("Test setNewCurrentTime() with null input")
    void testSetNewCurrentTimeWithNullInput() {
      // Act and Assert: Set a new current time with null input
      // and expect an exception
      IllegalArgumentException exception = assertThrows(
          IllegalArgumentException.class,
          () -> trainDepartureRegister.setNewCurrentTime(null),
          "Setting the new current time with null input should throw an IllegalArgumentException."
      );
      // Assert: Verify the exception type
      assertEquals(IllegalArgumentException.class, exception.getClass(),
          "The exception should be of type IllegalArgumentException.");
      // Assert: Verify the exception message
      assertEquals("New current time cannot be null.", exception.getMessage(),
          "The exception message should indicate that the new current time cannot be null.");
    }

    /**
     * Tests the {@code setNewCurrentTime()} method with an earlier time input.
     * Expects the current time to remain unchanged.
     */
    @Test
    @DisplayName("Test setNewCurrentTime() with earlier time input")
    void testSetNewCurrentTimeEarlier() {
      // Arrange
      LocalTime initialCurrentTime = LocalTime.of(12, 0);
      trainDepartureRegister.setNewCurrentTime(initialCurrentTime);

      // Act
      LocalTime newCurrentTime = LocalTime.of(11, 0);
      trainDepartureRegister.setNewCurrentTime(newCurrentTime);

      // Assert
      assertEquals(initialCurrentTime, trainDepartureRegister.getCurrentTime(),
          "The current time should remain unchanged if the new time is earlier.");
    }

    /**
     * Test getting the current time using the getCurrentTime method.
     */
    @Test
    @DisplayName("Test getCurrentTime()")
    void testGetCurrentTime() {
      // Arrange: Set a new current time
      LocalTime currentTime = LocalTime.now();
      trainDepartureRegister.setNewCurrentTime(currentTime);

      // Act and Assert: Get the current time and verify
      // it matches the previously set time
      assertEquals(currentTime, trainDepartureRegister.getCurrentTime(),
          "The current time should match the previously set time.");
    }
  }

  /**
   * Nested class for testing the getTrainDepartures method of the TrainDepartureRegister.
   */
  @Nested
  @DisplayName("Test get train departures")
  class TestGetTrainDepartures {

    /**
     * Test getting all train departures using the getTrainDepartures method.
     * The TrainDepartureRegister is properly initialized.
     * Positive scenario
     */
    @Test
    @DisplayName("Test get all train departures")
    void testGetTrainDepartures() {
      // Act: Get all train departures
      ArrayList<TrainDeparture> departures = trainDepartureRegister.getTrainDepartures();

      // Assert: Verify the size and presence of specific departures
      assertAll("Get all train departures",
          () -> assertEquals(4, departures.size(),
              "Incorrect number of departures found"),
          () -> assertTrue(departures.contains(newDeparture),
              "New departure not found in results"),
          () -> assertTrue(departures.contains(departure1),
              "Departure1 not found in results"),
          () -> assertTrue(departures.contains(departure2),
              "Departure2 not found in results"),
          () -> assertTrue(departures.contains(departure3),
              "Departure3 not found in results")
      );
    }

    /**
     * Test getting train departures when the register is empty.
     * Assumes that the TrainDepartureRegister is properly initialized.
     * Negative scenario
     */
    @Test
    @DisplayName("Test get train departures from an empty register")
    void testGetTrainDeparturesEmptyRegister() {
      // Arrange: Create a new empty TrainDepartureRegister
      TrainDepartureRegister emptyRegister = new TrainDepartureRegister();

      // Act: Get train departures from an empty register
      ArrayList<TrainDeparture> emptyDepartures = emptyRegister.getTrainDepartures();

      // Assert: Verify that the list is empty
      assertTrue(emptyDepartures.isEmpty(),
          "The list of departures should be empty for an empty register.");
    }
  }

  /**
   * Nested test class for testing some of the set methods in the TrainDepartureRegister class.
   */
  @Nested
  @DisplayName("TrainDepartureRegister set-methods Test")
  public class TestSetMethods {

    /**
     * Tests the setTrackFromUI() method in the TrainDepartureRegister class.
     */
    @Test
    @DisplayName("Test setTrackFromUI() method")
    void testSetTrackFromUI() {
      // Arrange
      trainDepartureRegister.addTrainDepartureFromUI(LocalTime.of(8, 30), "L1", 900, "Drammen");

      // Act
      trainDepartureRegister.setTrackFromUI(900, 5);

      // Assert
      assertEquals(5, trainDepartureRegister.findTrainDepartureByTrainNumber(900).getTrack(),
          "The track for train 900 should be set to 5");
    }

    /**
     * Tests the setDelayFromUI() method in the TrainDepartureRegister class.
     */
    @Test
    @DisplayName("Test setDelayFromUI() method")
    void testSetDelayFromUI() {
      // Arrange
      trainDepartureRegister.addTrainDepartureFromUI(LocalTime.of(8, 30), "P1", 200, "Moss");

      // Act
      trainDepartureRegister.setDelayFromUI(200, Duration.ofMinutes(15));

      // Assert
      assertEquals(Duration.ofMinutes(15),
          trainDepartureRegister.findTrainDepartureByTrainNumber(200).getDelay(),
          "The delay for train 200 should be set to 15 minutes");
    }
  }

  /**
   * Nested class for testing the toString() method of the TrainDepartureRegister.
   */
  @Nested
  @DisplayName("TrainDepartureRegister toString() Test")
  public class TestRegisterToString {

    /**
     * Test toString() method with train departures.
     */
    @Test
    @DisplayName("Test toString() with train departures")
    void testToStringWithTrainDepartures() {
      // Arrange: The initial state of the register is set up with multiple train departures

      // Act: Get the result of toString()
      String resultString = trainDepartureRegister.toString();

      // The string is sorted by when the train was added into the register, not sorted by time.
      String expectedString =
          """
              |  Departure Time      |  Line      |  Train Number   |  Destination         |  Delay     |  Track     |  Adjusted Departure Time |
              |---------------------------------------------------------------------------------------------------------------------------------|
              |  14:30               |  L2        |  602            |  Trondheim           |            |            |                          |
              |  14:00               |  L2        |  601            |  Trondheim           |            |            |                          |
              |  15:45               |  L3        |  603            |  Bergen              |            |            |                          |
              |  17:00               |  L4        |  604            |  Bergen              |            |            |                          |
              """;


      // Assert: Verify the result against the expected string
      assertEquals(expectedString, resultString,
          "toString() output does not match the expected string");
    }

    /**
     * Test toString() method with no train departures.
     */
    @Test
    @DisplayName("Test toString() with no train departures")
    void testToStringWithNoTrainDepartures() {
      // Arrange: Create a new TrainDepartureRegister with no train departures
      TrainDepartureRegister emptyRegister = new TrainDepartureRegister();

      // Act: Ge the result of toString()
      String resultString = emptyRegister.toString();

      String expectedString =
          """
              |  Departure Time      |  Line      |  Train Number   |  Destination         |  Delay     |  Track     |  Adjusted Departure Time |
              |---------------------------------------------------------------------------------------------------------------------------------|
              """;

      // Assert: Verify the result against the expected string
      assertEquals(expectedString, resultString,
          "toString() output does not match the expected string");
    }
  }
}
