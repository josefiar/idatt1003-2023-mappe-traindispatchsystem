package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * The TrainDispatchSystemUI class handles the user interface for the train dispatch system.
 * It provides menu options for interacting with the TrainDepartureRegister.
 */
public class TrainDispatchSystemUI {

  /** The TrainDepartureRegister instance to manage train departures. */
  public static TrainDepartureRegister trainDepartureRegister;

  /** Scanner for user input. */
  Scanner scanner;

  // Constants representing the different menu choices
  private final int PRESENT_ALL_TRAINS = 1;
  private final int ADD_NEW_TRAIN = 2;
  private final int CHANGE_TRAIN_TRACK = 3;
  private final int ADD_DELAY = 4;
  private final int FIND_TRAIN_BY_TNR = 5;
  private final int FIND_TRAIN_BY_DESTINATION = 6;
  private final int CHANGE_CURRENT_TIME = 7;
  private final int EXIT = 8;

  /**
   * Constructs a TrainDispatchSystemUI object.
   * Initializes the TrainDepartureRegister and Scanner for user input.
   */
  public TrainDispatchSystemUI() {
    trainDepartureRegister = new TrainDepartureRegister();
    scanner = new Scanner(System.in);
  }

  /**
   * Presents the menu for the user, awaits input, and returns the chosen menu option.
   * The menu options range from 1 to 8, with other inputs indicating an invalid input.
   *
   * @return The user's menu choice as a positive number starting from 1.
   */
  private int showMenu() {
    int menuChoice = 0;

    // Display the menu
    System.out.println("\n***** Train Departure Application v1.0 *****");
    System.out.println("Welcome to the TrainDeparture Application\n");
    System.out.println("""
        [1] Present all trains
        [2] Add a new train
        [3] Change train track
        [4] Add delay of a train
        [5] Find train by train number
        [6] Find train by destination
        [7] Change current time
        [8] Quit TrainDeparture Application
        """);
    System.out.println("\nPlease enter a number between 1 and 8.\n");

    // Read user input
    Scanner sc = new Scanner(System.in);
    if (sc.hasNextInt()) {
      menuChoice = sc.nextInt();
    } else {
      System.out.println("You must enter a number between 1 and 8, not text");
    }
    return menuChoice;
  }

  /**
   * Initiates and starts the Train Departure Application.
   * This method enters the main loop to interact with the user,
   * presenting the menu, retrieving the selected menu choice,
   * and executing the corresponding functionality.
   * The loop continues until the user chooses to exit the application.
   */
  public void start() {
    /*
     * The while-loop will run as long as the user has not selected
     * to quit the application
     */
    boolean finished = false;
    while (!finished) {
      int menuChoice = this.showMenu();

      // Switch statement to perform actions based on the user's menu choice
      switch (menuChoice) {
        case PRESENT_ALL_TRAINS -> presentAllTrains(); // Sort by departure time
        case ADD_NEW_TRAIN -> addNewTrain();
        case CHANGE_TRAIN_TRACK -> changeTrainTrack();
        case ADD_DELAY -> addDelay();
        case FIND_TRAIN_BY_TNR -> findTrainByTrainNumber();
        case FIND_TRAIN_BY_DESTINATION -> findTrainByDestination();
        case CHANGE_CURRENT_TIME -> changeCurrentTime();
        case EXIT -> finished = exitApplication();
        default -> System.out.println("Unrecognized menu selected..");
      }
    }
  }

  /**
   * Initializes the Train Departure System with sample train departures.
   * This method creates and adds several TrainDeparture instances
   * to the system for testing and demonstration purposes.
   */
  public static void init() {
    /*
     * Create instances of TrainDeparture and use addTrainDeparture to
     * the TrainDepartureRegister
     */
    List<String> lines = List.of("L1", "P1", "O1", "L1", "P1", "O1");
    List<Integer> trainNumbers = List.of(601, 681, 671, 600, 682, 672);
    List<String> destinations = List.of(
        "Bergen", "Oslo", "Trondheim", "Bergen", "Oslo", "Trondheim");
    List<LocalTime> departureTimes = List.of(
        LocalTime.of(12, 15),
        LocalTime.of(13, 25),
        LocalTime.of(11, 25),
        LocalTime.of(16, 0),
        LocalTime.of(20, 30),
        LocalTime.of(22, 45)
    );

    IntStream.range(0, lines.size())
        .forEach(i -> trainDepartureRegister.addTrainDepartureFromUI(
            departureTimes.get(i), lines.get(i), trainNumbers.get(i), destinations.get(i)
        ));
  }

  /**
   * Displays a sorted list of all train departures by departure time.
   * Retrieves and prints details, including a formatted header with column titles.
   */
  public void presentAllTrains() {
    // Print formatted header with column titles
    System.out.println(stringHeader());

    // Retrieve all train departures, sort them by departure time,
    // and print details to the console
    trainDepartureRegister.getAllDeparturesSortedByTime()
        .forEach(System.out::print);
  }

  /**
   * Adds a new train departure based on user input.
   * User is guided to input valid information for
   * departure time, line designation, train number, and destination.
   * If any input is invalid, the user is prompted again until valid input is provided.
   * After successfully creating a new train departure,
   *  it is added to the train departures register.
   */
  public void addNewTrain() {
    LocalTime departureTime = getValidatedDepartureTime();
    String line = getValidatedLine();
    int trainNumber = getValidatedTrainNumberInput();
    String destination = getValidatedDestination();

    // Create and add new train departure
    try {
      trainDepartureRegister.addTrainDepartureFromUI(departureTime, line, trainNumber, destination);
      System.out.println("\nThe new train has been added to the train departures.");
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      System.out.println("\nThe new train was not added.");
    }
  }

  /**
   * Allows user to change the track number for a specific train.
   * It prompts the user for the train number and the new track number,
   * validates the input, and updates the track if the input is valid.
   * The user receives feedback on the operation's success.
   * If any input is invalid or the specified train is not found,
   * appropriate error messages are displayed.
   */
  public void changeTrainTrack() {
    int trainNumberToChangeTrack = getValidatedTrainNumber();
    int newTrack = getValidatedNewTrack();

    try {
      trainDepartureRegister.setTrackFromUI(trainNumberToChangeTrack, newTrack);
      System.out.println("Track for train "
          + trainNumberToChangeTrack + " has been updated to: " + newTrack + ".");
    } catch (IllegalArgumentException e) {
      System.out.println("Failed to update track: " + e.getMessage());
    }
  }

  /**
   * Adds a delay to a train departure based on user input.
   * The delay is added to the specified train,
   * and the user receives feedback on the operation's success.
   * If any input is invalid or the specified train is not found,
   * appropriate error messages are displayed.
   */
  public void addDelay() {
    int trainNumberToAddDelay = getValidatedTrainNumber();
    Duration additionalDelay = getValidatedAdditionalDelay();

    try {
      trainDepartureRegister.setDelayFromUI(trainNumberToAddDelay, additionalDelay);
      System.out.println("Delay for train " + trainNumberToAddDelay + " has been updated.");
    } catch (IllegalArgumentException e) {
      System.out.println("Failed to add delay: " + e.getMessage());
    }
  }

  /**
   * Finds train with train number and
   * displays information about a train based on user input.
   * If the train is not found or if the input is invalid,
   * appropriate error messages are displayed.
   */
  public void findTrainByTrainNumber() {
    int trainNumberToFind = getValidatedTrainNumber();

    // Find and display the train with the specified train number
    try {
      TrainDeparture foundTrain
          = trainDepartureRegister.findTrainDepartureByTrainNumber(trainNumberToFind);
      displayTrainInformation(foundTrain);
    } catch (IllegalArgumentException e) {
      System.out.println("Failed to find train: " + e.getMessage());
      scanner.nextLine(); // Consume any remaining input
    }
  }

  /**
   * Finds and displays information about trains based on the destination entered by the user.
   * If no trains are found or if the input is invalid, appropriate error messages are displayed.
   */
  public void findTrainByDestination() {
    String destinationToFind = getValidatedDestination();

    try {
      ArrayList<TrainDeparture> foundTrains
          = trainDepartureRegister.findTrainDeparturesByDestination(destinationToFind);
      displayFoundTrains(destinationToFind, foundTrains);
    } catch (IllegalArgumentException e) {
      System.out.println("Failed to find trains: " + e.getMessage());
      scanner.nextLine(); // Consume any remaining input
    }
  }

  /**
   * Changes the current time for the train departure system.
   * Prompts the user to enter a new current time in the format HHmm and updates
   * the system's current time if the input is valid;
   * otherwise, appropriate error handling is performed.
   */
  public void changeCurrentTime() {
    LocalTime newCurrentTime = getValidatedNewCurrentTime();

    try {
      trainDepartureRegister.setNewCurrentTime(newCurrentTime);
      System.out.println("Current time is: " + trainDepartureRegister.getCurrentTime());
    } catch (IllegalArgumentException e) {
      // Handle exception if needed
    }
  }

  /**
   * Displays a farewell message to the user
   * and signals the intent to exit the TrainDeparture Application.
   *
   * @return Always returns true to indicate the request to exit the application.
   */
  public boolean exitApplication() {
    System.out.println("Thank you for using the TrainDeparture Application!\n");
    return true;
  }

  /**
   * Gets and validates the departure time input from the user.
   *
   * @return The validated departure time as a LocalTime object.
   */
  private LocalTime getValidatedDepartureTime() {
    boolean answered = false;
    LocalTime departureTime = null;
    
    do {
      System.out.print("Enter the departure time in the format HHmm: ");

      try {
        String timeInput = scanner.nextLine();
        // Parse the input string to LocalTime
        departureTime = LocalTime.parse(timeInput, DateTimeFormatter.ofPattern("HHmm"));
        answered = true;
      } catch (DateTimeParseException e) {
        // If an exception occurs, do nothing and continue to the next iteration
      }
    } while (!answered);
    
    return departureTime;
  }

  /**
   * Gets and validates the line designation input from the user.
   *
   * @return The validated line designation as a String.
   */
  private String getValidatedLine() {
    boolean answered = false;
    String line = null;
    do {
      System.out.print("Enter the line designation for the train route: ");

      try {
        line = scanner.nextLine();
        if (!line.isBlank()) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        scanner.nextLine();
      }
    } while (!answered);

    return line;
  }

  /**
   * Gets and validates the train number input from the user.
   *
   * @return The validated train number as an Integer.
   */
  private int getValidatedTrainNumberInput() {
    int trainNumber = 0;
    boolean answered = false;

    do {
      System.out.println("Enter the train number: ");

      try {
        trainNumber = Integer.parseInt(scanner.nextLine());
        if (trainNumber > 0) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        // Consume invalid input
      }
    } while (!answered);

    return trainNumber;
  }


  /**
   * Gets and validates the destination input from the user.
   *
   * @return The validated destination as a String.
   */
  private String getValidatedDestination() {
    boolean answered = false;
    String destination = null;
    do {
      System.out.print("Enter the destination: ");

      try {
        destination = scanner.nextLine();
        if (!destination.isBlank()) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        // Consume invalid input
      }
    } while (!answered);

    return destination;
  }

  /**
   * Prompts the user for the train number, validates the input,
   * and ensures that the entered train number exists in the train departure register.
   *
   * @return The validated train number entered by the user.
   */
  private int getValidatedTrainNumber() {
    int trainNumber = 0;
    boolean answered = false;

    do {
      System.out.println("Enter the train number: ");

      try {
        trainNumber = Integer.parseInt(scanner.nextLine());
        if (trainDepartureRegister.findTrainDepartureByTrainNumber(trainNumber) != null) {
          answered = true;
        } else {
          System.out.println("Train with number " + trainNumber + " not found.");
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        // Consume invalid input
      }
    } while (!answered);

    return trainNumber;
  }


  /**
   * Prompts the user for the new track number, validates the input,
   * and ensures that the entered track number is either positive or equal to -1.
   *
   * @return The validated new track number entered by the user.
   */
  private int getValidatedNewTrack() {
    int newTrack = -1;
    boolean answered = false;

    do {
      System.out.println("Enter the new track number: ");
      try {
        newTrack = Integer.parseInt(scanner.nextLine());
        if (newTrack > 0 || newTrack == -1) {
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        // Consume invalid input
      }
    } while (!answered);

    return newTrack;
  }

  /**
   * Gets and validates the additional delay input from the user.
   *
   * @return The additional delay as a Duration object.
   */
  private Duration getValidatedAdditionalDelay() {
    int delayMinutes;
    boolean answered = false;
    Duration additionalDelay = null;

    do {
      System.out.println("Enter the additional delay in minutes: ");
      try {
        delayMinutes = Integer.parseInt(scanner.nextLine());
        if (delayMinutes > 0) {
          additionalDelay = Duration.ofMinutes(delayMinutes);
          answered = true;
        }
      } catch (InputMismatchException | IllegalArgumentException e) {
        // Consume invalid input
      }
    } while (!answered);

    return additionalDelay;
  }

  /**
   * Prompts the user to enter and validates a new current time in the format HHmm.
   *
   * @return The validated LocalTime object representing the new current time.
   */
  private LocalTime getValidatedNewCurrentTime() {
    LocalTime newCurrentTime = null;
    boolean answered = false;
    String timeInput;

    do {
      System.out.print("Enter the new current time in format HHmm: ");

      try {
        timeInput = scanner.nextLine();
        if (!timeInput.isBlank()) {
          newCurrentTime = LocalTime.parse(timeInput, DateTimeFormatter.ofPattern("HHmm"));
          answered = true;
        }
      } catch (DateTimeParseException e) {
        // Consume invalid input
      }
    } while (!answered);

    return newCurrentTime;
  }

  /**
   * Displays information about a found train.
   *
   * @param foundTrain The TrainDeparture object to display information about.
   */
  private void displayTrainInformation(TrainDeparture foundTrain) {
    if (foundTrain != null && trainDepartureRegister.getTrainDepartures().contains(foundTrain)) {
      System.out.println("Train found: \n");
      System.out.println(stringHeader());
      System.out.print(foundTrain);
    } else {
      System.out.println("Train not found.");
    }
  }

  /**
   * Displays information about the found trains.
   *
   * @param destinationToFind The destination entered by the user.
   * @param foundTrains       The list of found TrainDeparture objects.
   */
  private void displayFoundTrains(String destinationToFind, ArrayList<TrainDeparture> foundTrains) {
    if (!foundTrains.isEmpty()) {
      System.out.println("Trains found for destination " + destinationToFind + ":");
      System.out.println(stringHeader());
      foundTrains.forEach(System.out::print);
    } else {
      System.out.println("No trains found for destination " + destinationToFind + ".");
    }
  }

  /**
   * Generates a formatted header string for the train departures, including column names.
   *
   * @return A formatted string containing column names for the train departures.
   */
  public String stringHeader() {
    String sbHeader = "";
    sbHeader += (String.format(
        "%-1s%-20s%-1s%-10s%-1s%-15s%-1s%-20s%-1s%-10s%-1s%-10s%-1s%-20s%-1s%n",
        "|  ",
        "Departure Time", "|  ",
        "Line", "|  ",
        "Train Number", "|  ",
        "Destination", "|  ",
        "Delay", "|  ",
        "Track", "|  ", "Adjusted Departure Time ", "|"));
    sbHeader += ("|-----------------------------------------------------"
        + "----------------------------------------------------------------------------|");

    return sbHeader;
  }
}