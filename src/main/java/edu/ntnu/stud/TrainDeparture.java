package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Represents a train departure with details such as departure time, line, train number,
 * destination, delay, and track assignment.
 */
public final class TrainDeparture {

  private LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private Duration delay;
  private int track;

  /**
   * Constructs a new TrainDeparture with the specified parameters.
   *
   * @param departureTime The departure time of the train in the "HH:mm" format (24-hour clock).
   * @param line          The line designation for the train route (e.g., "L1", "F4").
   * @param trainNumber   The unique train number within the same day.
   * @param destination   The destination of the train.
   * @throws IllegalArgumentException Thrown if any parameter is invalid:
   *                                     - If departureTime is null or in an incorrect format.
   *                                     - If line is null or blank.
   *                                     - If trainNumber is a non-positive number.
   *                                     - If destination is null or blank.
   */
  public TrainDeparture(LocalTime departureTime, String line, int trainNumber,
                        String destination) throws IllegalArgumentException {

    // Check if the departureTime empty
    if (departureTime == null) {
      throw new IllegalArgumentException("Invalid departure time");
    }

    // Check if the line is empty
    if (line == null || line.isBlank()) {
      throw new IllegalArgumentException("Line cannot be empty");
    }

    // Check if the destination is empty
    if (destination == null || destination.isBlank()) {
      throw new IllegalArgumentException("Destination cannot be empty");
    }

    // Check if the train number is in correct interval
    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Invalid train number. "
          + "Has to be a non-negative number over 0.");
    }

    // Set the values if validation passes
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.delay = Duration.ofMinutes(0); // No delay by default
    this.track = -1; // No train tracks assigned by default
  }

  /**
   * Checks whether the train departure has occurred based on the current time.
   * If a delay is present,
   * the method considers the adjusted departure time (original departure time + delay).
   *
   * @param currentTime The current time used for comparison.
   * @return True if the train has departed; false otherwise.
   */
  public boolean isDeparted(LocalTime currentTime) {
    // If delay is present, consider the departure time with delay
    return !currentTime.isBefore(getAdjustedDepartureTime());
  }

  /**
   * Gets the adjusted departure time, accounting for any delay.
   *
   * @return The adjusted departure time, considering any delay.
   */
  public LocalTime getAdjustedDepartureTime() {
    return departureTime.plusMinutes(delay.toMinutes());
  }

  /**
   * Gets the departure time of the train.
   *
   * @return The departure time.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the line designation for the train route.
   *
   * @return The line designation.
   */
  public String getLine() {
    return line;
  }

  /**
   * Gets the unique train number.
   *
   * @return The train number.
   */
  public int  getTrainNumber() {
    return trainNumber;
  }

  /**
   * Gets the destination of the train.
   *
   * @return The destination.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Gets the delay of the train.
   *
   * @return The delay.
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Gets the track assigned to the train.
   *
   * @return The track number, or -1 if no track is assigned.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Sets current departure time with new delay.
   */
  public void addDelayToDepartureTime() { // set departureTime to new delayed time
    departureTime = departureTime.plusMinutes(delay.toMinutes());
  }

  /**
   * Sets additional delay to the current delay time.
   *
   * @param additionalDelay The extra delay to be added to the existing delay.
   * @throws IllegalArgumentException If additionalDelay is negative.
   */
  public void setDelay(Duration additionalDelay) throws IllegalArgumentException {
    if (additionalDelay.isNegative()) {
      throw new IllegalArgumentException("Additional delay cannot be negative.");
    }
    delay = delay.plusMinutes(additionalDelay.toMinutes());
  }

  /**
   * Sets the track assigned to the train.
   *
   * @param track The new track number.
   * @throws IllegalArgumentException Thrown if the track number is negative.
   */
  public void setTrack(int track) throws IllegalArgumentException {
    if (track > 0 || track == -1) {
      this.track = track;
    } else {
      throw new IllegalArgumentException(
          "Invalid track number. Has to be a positive number or -1.");
    }
  }

  /**
   * Provides a string representation of the train departure details.
   *
   * @return A string with an overview of the train departures.
   */
  @Override
  public String toString() {
    return String.format("%-1s%-20s%-1s%-10s%-1s%-15s%-1s%-20s%-1s%-10s%-1s%-10s%-1s%-20s%-1s%n",
        "|  ",
        departureTime, "|  ",
        line, "|  ",
        trainNumber, "|  ",
        destination, "|  ",
        (delay.isZero() ? "" : (delay.toMinutes() / 60 + "h, " + delay.toMinutes() % 60 + "m")),
        "|  ",
        (track == -1 ? "" : track), "|   ", (delay.isZero() ? "" : getAdjustedDepartureTime()),
        "   |");
  }
}