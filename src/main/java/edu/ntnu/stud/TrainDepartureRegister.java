package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * TrainDepartureRegister is a class representing a register of train departures.
 * It manages train departure instances and provides functionality for adding,
 * searching, and manipulating train departures.
 * Version 1.0
 * Date: 12.12.23
 */
public class TrainDepartureRegister {

  /** List to store train departures. */
  private ArrayList<TrainDeparture> trainDepartures;
  private LocalTime currentTime;

  /**
   * Constructs a new TrainDepartureRegister with an empty list of train departures.
   */
  public TrainDepartureRegister() {
    this.trainDepartures = new ArrayList<>();
    this.currentTime = LocalTime.of(0, 0);
  }

  /**
   * Adds a new train departure to the register.
   *
   * @param newDeparture The TrainDeparture instance to be added.
   * @return {@code true} if the train departure is successfully added, {@code false} otherwise.
   * @throws IllegalArgumentException If a train with the same train number already exists.
   */
  public boolean addTrainDeparture(TrainDeparture newDeparture) throws IllegalArgumentException {
    /*
     * Check for duplicates, if not, add departure
     */
    boolean exists = trainDepartures.stream()
        .anyMatch(train -> train.getTrainNumber() == newDeparture.getTrainNumber());

    if (!exists) {
      trainDepartures.add(newDeparture);
      return true;
    } else {
      throw new IllegalArgumentException("Train departure "
          + "with the same train number already exists.");
    }
  }

  /**
   * Adds a train departure to the system from user interface input.
   *
   * @param departureTime The scheduled departure time.
   * @param line The train line.
   * @param trainNumber The unique train number.
   * @param destination The destination.
   */
  public void addTrainDepartureFromUI(LocalTime departureTime, String line, int trainNumber,
                                         String destination) {
    TrainDeparture tempTD = new TrainDeparture(departureTime, line, trainNumber, destination);
    addTrainDeparture(tempTD);
  }

  /**
   * Finds a train departure by its train number.
   *
   * @param trainNumber The train number to search for.
   * @return The TrainDeparture instance with the specified train number, or null if not found.
   */
  public TrainDeparture findTrainDepartureByTrainNumber(int trainNumber) {
    return trainDepartures.stream()
                          .filter(train -> train.getTrainNumber() == trainNumber)
                          .findFirst()
                          .orElse(null);
  }

  /**
   * Finds train departures based on the destination.
   *
   * @param destination The destination to search for.
   * @return List of TrainDeparture instances with the specified destination.
   * @throws IllegalArgumentException If no trains are found for the given destination.
   */
  public ArrayList<TrainDeparture> findTrainDeparturesByDestination(String destination)
      throws IllegalArgumentException {

    // Filters the list of train departures based on the provided destination, ignoring case.
    ArrayList<TrainDeparture> foundDepartures = trainDepartures.stream()
        .filter(train -> train.getDestination().equalsIgnoreCase(destination))
        .collect(Collectors.toCollection(ArrayList::new));

    // Throws an IllegalArgumentException if no trains are found for the given destination.
    if (foundDepartures.isEmpty()) {
      throw new IllegalArgumentException("Could not find any trains going to destination "
          + destination);
    }

    return foundDepartures;
  }

  /**
   * Removes train departures that have departed based on the current time.
   * Train departures with a departure time earlier than the current time are removed.
   * The removal is done directly on the internal list.
   */
  private void removeDepartedDepartures() {
    trainDepartures.removeIf(train -> train.isDeparted(currentTime));
  }

  /**
   * Gets all train departures sorted by departure time.
   *
   * @return List of TrainDeparture instances sorted by departure time.
   */
  public ArrayList<TrainDeparture> getAllDeparturesSortedByTime() {
    this.trainDepartures = trainDepartures.stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .collect(Collectors.toCollection(ArrayList::new));
    // Takes each instance of a TrainDeparture object, and compares departure times.
    return trainDepartures;
  }

  /**
   * Sets a new current time for the train departure register.
   *
   * @param newCurrentTime The new current time.
   * @throws IllegalArgumentException If the new current time is null.
   */
  public void setNewCurrentTime(LocalTime newCurrentTime) throws IllegalArgumentException {
    if (newCurrentTime == null) {
      throw new IllegalArgumentException("New current time cannot be null.");
    }
    if (newCurrentTime.isAfter(this.currentTime)) {
      this.currentTime = newCurrentTime;
      removeDepartedDepartures();
    }
  }

  /**
   * Sets the track assigned to a train from the user interface.
   *
   * @param trainNumber The unique train number.
   * @param track The new track number.
   */
  public void setTrackFromUI(int trainNumber, int track) {
    findTrainDepartureByTrainNumber(trainNumber).setTrack(track);
  }

  /**
   * Sets additional delay to a train from the user interface.
   *
   * @param trainNumber the unique train number.
   * @param delay The extra delay to be added to the existing delay.
   */
  public void setDelayFromUI(int trainNumber, Duration delay) {
    findTrainDepartureByTrainNumber(trainNumber).setDelay(delay);
  }

  /**
   * Gets the current time.
   *
   * @return The current time.
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Gets the list of train departures in the register.
   *
   * @return The list of TrainDeparture instances.
   */
  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  /**
   * Generates a string representation of the train departures, including departure time,
   * line designation, train number, destination, delay, track, and adjusted departure time.
   *
   * @return A formatted string with an overview of the train departures.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format("%-1s%-20s%-1s%-10s%-1s%-15s%-1s%-20s%-1s%-10s%-1s%-10s%-1s%-20s%-1s%n",
        "|  ",
        "Departure Time", "|  ",
        "Line", "|  ",
        "Train Number", "|  ",
        "Destination", "|  ",
        "Delay", "|  ",
        "Track", "|  ", "Adjusted Departure Time ", "|"));
    sb.append("|-----------------------------------------------------"
        + "----------------------------------------------------------------------------|\n");

    String result = trainDepartures.stream()
        .map(TrainDeparture::toString)
        .collect(Collectors.joining());

    sb.append(result);
    return sb.toString();
  }
}
