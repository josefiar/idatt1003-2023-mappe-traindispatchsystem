package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 * It contains the main method to start the application.
 */
public class TrainDispatchApp {

  /**
   * The main method to start the train dispatch application.
   *
   * @param args The command line arguments (not used in this application).
   */
  public static void main(String[] args) {
    // Create an instance of the TrainDispatchSystemUI
    TrainDispatchSystemUI ui = new TrainDispatchSystemUI();

    // Initialize the TrainDispatchSystemUI
    TrainDispatchSystemUI.init();

    // Start the user interface
    ui.start();
  }
}
